import fs from 'fs';
import path from 'path';
//import multiparty  from 'multiparty';
// new multiparty.Form().parse(req, fuction(err, fields, files){})

const app = require('express')();
// 尝试上传文件
const save_file_promise = (filename, buffer) => {
    
    // 上传到根目录
    const root = '../';
    const file_path = path.resolve(__dirname, '../'+filename);
    console.log(`正在保存文件:${file_path}`);
    return new Promise((rs, rj) => {
        fs.writeFile(file_path, buffer, 'binary', err => {
            if(err) {
                rj(err);
            } else {
                rs(filename);
            }
        });
    });
};


const handler = (req, res) => {
        if(!req.headers['content-type']) {
            return res.status(501).end('无法处理的请求！');
        }
        
        var boundary = req.headers['content-type'].split(';')[1].replace(/^\s+boundary=/, '');
        console.log('读取到boundary:'+boundary);
        // 直接解析二进制Buffer数据
        var body = Buffer.from('');
        req.on('data', chunk => {
            body = Buffer.concat([body, chunk]);
        });
        
        req.on('end', () => {
            let bp = 0;
            let boundariy_fields = [];
            boundary  = '--' + boundary;
            while(body.indexOf(boundary) >= 0) {
                // 尾部的boundary之后获取不到不用管
                let sp = body.indexOf(boundary);
                boundariy_fields.push(body.slice(0, sp));
                body = body.slice(sp + boundary.length);
            }
            
            boundariy_fields.shift();   // 去掉首部的

            // 然后对中间的进行解析
            // 去掉首尾部的\r\n
            let form_data = boundariy_fields.reduce((prev, buffer) => {
                // 4个\r\n可以区隔开(disposition，type)和value
                let dvn = buffer.indexOf('\r\n\r\n');
                // 去掉开头和末尾的\r\n
                var disposition = buffer.slice(2, dvn);
                var value = buffer.slice(dvn + 4, -2);

                // 对disposition进行解析,disposition全部是string类型所以不用担心二进制转化为string会出问题
                // 包含其他属性
                let metas = [];
                let k = disposition.toString();
                let n = k.indexOf('\r\n');
                if (n > 0) {
                    let props = k.split('\r\n').slice(1);
                    metas.push(props);
                    k = k.substr(0, n);
                }
                let [name, filename] = k.split(';').slice(1);
                name = name?.match(/name="(\S+)"/)?.[1];
                filename = filename?.match(/filename="(\S+)"/)?.[1];
                if(filename) {
                    prev.files.push({name, value, filename, metas});
                } else {
                    // 可能有重复的name，比如传入数组会是类似arr[]，可以自行处理
                    prev.fields.push({name, value});
                }
                console.log({name, value: value.slice(0,20),filename});
                return {fields: prev.fields, files: prev.files};
            }, {fields: [], files: []});

            res.setHeader('content-type', 'text/html; charset=utf-8');
            // 对form_data里面files进行保存
            Promise.all(
                form_data.files.map(file => save_file_promise(file.filename, file.value))
            ).then((...promises) => {
                res.end(`文件:${promises.join(',')}保存成功ok`);
            }).catch(err => {
                res.status(500).end('保存失败！');
            });
        });
}

app.post('/api/upload', handler);
module.exports = app;