import fetch from 'node-fetch';
const app = require('express')();
//const { v4 } = require('uuid');

app.get('/api/proxy', (req, res) => {
  if (req.query.url) {
      var url = req.query.url;
      if(!(/^http/).test(url)) {
        url = 'http://'+url;
      }

      fetch(url).then(res2 => { 
        var exclude_headers = ['content-encoding', 'content-length'];
        var headers = res2.headers;
        headers.forEach((v, k) => {
          if(exclude_headers.includes(k)) {
            return;
          }
          res.setHeader(k, v);
        });
        res.setHeader('Access-Control-Allow-Origin', '*');
        return res2.text()})
        .then(res2 => {
        res.end(res2);
      }).catch(err => {
          res.setHeader('content-type', 'text/html; charset=utf-8');
          res.status(500).end('<h2>Error:</h2>'+JSON.stringify(err));
      });

  } else {
    res.setHeader('Content-Type', 'text/html');
    res.setHeader('Cache-Control', 's-max-age=1, stale-while-revalidate');
    res.end(`404 Not Found<br/><hr/>Nginx1.2`);
  }
  
});

module.exports = app;