import sha1 from 'sha1';
const app = require('express')();

// 微信公众号的接口
app.get('/api/wx-zard', (req, res) => {
    const {signature, timestamp, nonce, echostr} = req.query;
    const token = "ZARD";
    if(signature && sha1([token, timestamp, nonce].sort().join('')) == signature) {
        return res.end(echostr);
    } else {
        res.setHeader('content-type', 'text/html; charset=utf-8');
        res.status(403).end('非微信访问，禁止访问！');
    }
    
});

module.exports = app;