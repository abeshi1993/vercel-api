import fetch from 'node-fetch';
const app = require('express')();
const { v4 } = require('uuid');

app.get('/api', (req, res) => {
  res.setHeader('content-type', 'text/html; charset=utf-8');
  res.end('访问了根目录');
});
app.get('/api/index', (req, res) => {
  if (req.query.url) {
      var url = req.query.url;
      if(!(/^http/).test(url)) {
        url = 'http://'+url;
      }
      fetch(url).then(res2 => { 
        var exclude_headers = ['content-encoding'];
        var headers = res2.headers;
        headers.forEach((v, k) => {
          if(exclude_headers.includes(k)) {
            return;
          }
          console.log('设置', k);
          res.setHeader(k, v);
        });
        return res2.text()})
        .then(res2 => {
        res.end(res2);
      }).catch(err => {
        console.log(err);
          res.end('Error:'+JSON.stringify(err));
      });

  } else {
    res.setHeader('Content-Type', 'text/html');
    res.setHeader('Cache-Control', 's-max-age=1, stale-while-revalidate');
    res.end(`404 Not Found<br/><hr/>Nginx1.2`);
  }
  
});

module.exports = app;